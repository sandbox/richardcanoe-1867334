-- CONTENTS --

*Summary
*Requirements
*Installation
*Contact

-- SUMMARY --
With this filter, a view may be created of just top level comments, for
 instance. Useful in a freemium model, allowing free access to comments
 and limited access to replies.

-- REQUIREMENTS --

Views Module.
Comments Module.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONTACT --

Current Maintainer: Richard Grieve <richardcanoe@gmail.com>
