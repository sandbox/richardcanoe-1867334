<?php
/**
 * @file
 * Definition of views_handler_filter_comment_depth.
 */

/**
 * Filter handler to check for depth of comment tree.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_comment_depth extends views_handler_filter_numeric {

  function op_simple($field) {
    $snippet = "(LENGTH(comment.thread) - LENGTH(REPLACE(comment.thread, '.', ''))) " . $this->operator . " :val";
    $arg[':val'] = $this->value['value'];
    $this->query->add_where_expression($this->options['group'], $snippet, $arg);
  }

  function op_between($field) {
    if ($this->operator == 'between') {
      $snippet = "(LENGTH(comment.thread) - LENGTH(REPLACE(comment.thread, '.', ''))) " . $this->operator  . " :min AND :max";
      $arg[':min'] = $this->value['min'];
      $arg[':max'] = $this->value['max'];
      $this->query->add_where_expression($this->options['group'], $snippet, $arg);
    }
    else {
      $snippet = "(LENGTH(comment.thread) - LENGTH(REPLACE(comment.thread, '.', ''))) <= :min OR (LENGTH(comment.thread) - LENGTH(REPLACE(comment.thread, '.', ''))) >= :max";
      $arg[':min'] = $this->value['min'];
      $arg[':max'] = $this->value['max'];
      $this->query->add_where_expression($this->options['group'], $snippet, $arg);
    }
  }

  function op_regex($field) {
    $snippet = "(LENGTH(comment.thread) - LENGTH(REPLACE(comment.thread, '.', ''))) RLIKE :val";
    $arg[':val'] = $this->value['value'];
    $this->query->add_where_expression($this->options['group'], $snippet, $arg);
  }
}
